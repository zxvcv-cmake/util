CMake toolbox
=============
Utilites for CMake files.

Quick start:
------------
Import example:
```
if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan_package_info.cmake")
   message(STATUS "Downloading conan_package_info.cmake from https://gitlab.com/zxvcv-cmake/util")
   file(DOWNLOAD "https://gitlab.com/zxvcv-cmake/util/-/raw/master/conan/conan_package_info.cmake"
                 "${CMAKE_BINARY_DIR}/conan_package_info.cmake")
endif()
include(${CMAKE_BINARY_DIR}/conan_package_info.cmake)

# <now use functions from this module>
```
