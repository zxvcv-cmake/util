# Copyright (c) 2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file


macro(generate_mock_library COMPONENT_NAME COMPONENT_HEADER_FILES DEPENDENCY_INCLUDES
                            COMPILE_DEFINITONS COMPONENT_INCLUDE_PATHS MOCK_LIBRARY)
    #[[ Function will generate mock files and create library for passed component data.

        [in] COMPONENT_NAME (str): Name of the component.
        [in] COMPONENT_HEADER_FILES (List(str)): List of absolute paths to all components' header files.
        [in] DEPENDENCY_INCLUDES (List(str)): List includes that should be passed into preprocessor.
        [in] COMPILE_DEFINITONS (List(str)): List compile definitions that should be passed into preprocessor.
        [in] COMPONENT_INCLUDE_PATHS (List(str)): List of paths that contains other files included in COMPONENT_HEADER_FILES.
        [out] MOCK_LIBRARY (str): generated mock library name
    ]]

    # prepare mock generator
    set(MOCK_FILES_DIR mocks/${COMPONENT_NAME})
    set(MOCK_HEADER_PATH ${MOCK_FILES_DIR}/mock_${COMPONENT_NAME}.hpp)
    set(MOCK_SOURCE_PATH ${MOCK_FILES_DIR}/mock_${COMPONENT_NAME}.cpp)

    set(MOCK_FILE_PREFIX mock_${COMPONENT_NAME})
    set(MOCK_CLASS_SUFFIX _${COMPONENT_NAME})

    # TODO[PP]: test what if there will be more than one header file in source package

    add_custom_command(
        OUTPUT ${MOCK_HEADER_PATH} ${MOCK_SOURCE_PATH}
        COMMAND python3 -m cmock
                        ${COMPONENT_HEADER_FILES}
                        --mock_file_name ${MOCK_FILE_PREFIX}
                        --mock_suffix_name ${MOCK_CLASS_SUFFIX}
                        --destination ${MOCK_FILES_DIR}
                        --header-destination ${MOCK_FILES_DIR}
                        --includes ${DEPENDENCY_INCLUDES}
                        --defines ${COMPILE_DEFINITONS}
        DEPENDS ${COMPONENT_HEADER_FILES}
        VERBATIM
    )

    # create mock library
    set(MOCK_LIBRARY_NAME mock_${COMPONENT_NAME})

    add_library(${MOCK_LIBRARY_NAME} EXCLUDE_FROM_ALL
        ${MOCK_SOURCE_PATH}
    )
    target_include_directories(${MOCK_LIBRARY_NAME} PUBLIC
        ${CMAKE_BINARY_DIR}/${MOCK_FILES_DIR}
        ${COMPONENT_INCLUDE_PATHS}
        ${DEPENDENCY_INCLUDES}
    )
    target_link_libraries(${MOCK_LIBRARY_NAME} PUBLIC
        gmock_maind
    )
    target_compile_definitions(${MOCK_LIBRARY_NAME} PUBLIC
        ${COMPILE_DEFINITONS}
    )

    set(${MOCK_LIBRARY} ${MOCK_LIBRARY_NAME})
endmacro()
