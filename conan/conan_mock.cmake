# Copyright (c) 2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file


macro(generate_mock_conan_libraries MOCK_GENERATE_PCKG_LIST MOCK_LIBRARIES)
    #[[ Function will generate mock libraries for each conan package passed in
        MOCK_GENERATE_PCKG_LIST.

        [in] MOCK_GENERATE_PCKG_LIST (List(str)): conan package names to generate mocks for
        [out] MOCK_LIBRARIES (List(str)): list of names of generated mock libraries
    ]]

    if(NOT EXISTS "${CMAKE_BINARY_DIR}/mock.cmake")
        message(STATUS "Downloading mock.cmake from https://gitlab.com/zxvcv-cmake/util")
        file(DOWNLOAD "https://gitlab.com/zxvcv-cmake/util/-/raw/master/common/mock.cmake"
                      "${CMAKE_BINARY_DIR}/mock.cmake")
    endif()
    include(${CMAKE_BINARY_DIR}/mock.cmake)

    foreach(PACKAGE_NAME ${MOCK_GENERATE_PCKG_LIST})
        get_conan_package_include_path(${PACKAGE_NAME} PACKAGE_INCLUDE_PATH)

        get_conan_package_packages_dependency(${PACKAGE_NAME} PACKAGES_DEPENDENCY_LIST)
        foreach(PCKG ${PACKAGES_DEPENDENCY_LIST})
            get_conan_package_include_path(${PCKG} PACKAGE_DEPENDENCY_INCLUDE_PATH)
            list(APPEND DEPENDENCY_INCLUDES ${PACKAGE_DEPENDENCY_INCLUDE_PATH})
        endforeach()

        string(TOUPPER ${PACKAGE_NAME} PACKAGE_NAME_UPPERCASE)
        set(COMPILE_DEFINITONS ${CONAN_COMPILE_DEFINITIONS_${PACKAGE_NAME_UPPERCASE}})

        file(GLOB PACKAGE_HEADER_FILES "${PACKAGE_INCLUDE_PATH}/*.h")

        generate_mock_library(${PACKAGE_NAME}
            "${PACKAGE_HEADER_FILES}"
            "${DEPENDENCY_INCLUDES}"
            "${COMPILE_DEFINITONS}"
            "${PACKAGE_INCLUDE_PATH}"
            MOCK_LIBRARY
        )

        list(APPEND CREATED_MOCK_LIBRARIES
            ${MOCK_LIBRARY}
        )
    endforeach()

    set(MOCK_LIBRARIES "${CREATED_MOCK_LIBRARIES}")
endmacro()
