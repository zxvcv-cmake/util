# Copyright (c) 2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file


function(get_conan_package_include_path PACKAGE_NAME PACKAGE_INCLUDE_PATH)
    #[[ Function will return include directory path for PACKAGE_NAME
        This package needs to be included in conan REQUIRES list.

        [in] PACKAGE_NAME (str): name of the package which include path will be returned
        [out] PACKAGE_INCLUDE_PATH (str): return placeholder for include directory path
    ]]

    # Filtering from conan include directories  that one which comes from PACKAGE_NAME
    set(_PACKAGE_INCLUDE_PATH ${CONAN_INCLUDE_DIRS})
    list(FILTER _PACKAGE_INCLUDE_PATH INCLUDE REGEX ".conan/data/${PACKAGE_NAME}/")

    # This should always return single path (there could be many .h files but should be
    #  in single include directory)
    list(REMOVE_DUPLICATES _PACKAGE_INCLUDE_PATH)
    list(LENGTH _PACKAGE_INCLUDE_PATH _TEMP)

    if(NOT (${_TEMP} EQUAL 1) )
        message(FATAL_ERROR "One package cannot have more than one include directories - ${PACKAGE_NAME}")
    endif()

    set(${PACKAGE_INCLUDE_PATH} ${_PACKAGE_INCLUDE_PATH} PARENT_SCOPE)
endfunction()


function(get_conan_package_packages_dependency PACKAGE_NAME PACKAGES_DEPENDENCY_LIST)
    #[[ Function will return list of package names that are dependency for passed package

        [in] PACKAGE_NAME (str): name of the package which dependent packages will be returned
        [out] PACKAGES_DEPENDENCY_LIST (str): list of package names that are dependency
            for passed package
    ]]

    get_conan_package_include_path(${PACKAGE_NAME} PACKAGE_INCLUDE_PATH)
    get_filename_component(PACKAGE_PATH ${PACKAGE_INCLUDE_PATH} DIRECTORY)
    execute_process(COMMAND python3 -m cmock.tools.get_require_package_names ${PACKAGE_PATH}/conaninfo.txt
        OUTPUT_VARIABLE _PACKAGES_DEPENDENCY_LIST
    )
    list(LENGTH _PACKAGES_DEPENDENCY_LIST _TEMP)
    if(NOT (${_TEMP} EQUAL 0) )
        string(REPLACE " " ";" _PACKAGES_DEPENDENCY_LIST ${_PACKAGES_DEPENDENCY_LIST})
    endif()
    set(PACKAGES_DEPENDENCY_LIST ${_PACKAGES_DEPENDENCY_LIST} PARENT_SCOPE)
endfunction()
