Changelog
=========

0.1.2 (2022-10-07)
------------------
- Add macro for mock generation (conan mock generator uses it).

0.1.1 (2022-09-26)
------------------
- Add macro for mocs generation.

0.1.0 (2022-09-26)
------------------
- Initial test package.
